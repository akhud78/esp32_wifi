# esp32_wifi
Wi-Fi component

## Setup
- Add [wi-fi component](https://gitlab.com/akhud78/esp32_wifi) into `components` folder.
```
$ cd ~/esp/my_project/components
$ git submodule add https://gitlab.com/akhud78/esp32_wifi wifi
```


## Configuration

- Set Wi-Fi Station Configuration
```
(Top) -> Component config -> WiFi Station Configuration
(myssid) WiFi SSID
(mypassword) WiFi Password
(5) Maximum retry
```
- WiFi Access Point Configuration
```
(Top) -> Component config -> WiFi Access Point Configuration
(myssid) WiFi SSID
(mypassword) WiFi Password
(1) WiFi Channel
(4) Maximal STA connections
```


